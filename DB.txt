Digital Ocean Rethink Tutorial: https://www.andrewmunsell.com/blog/install-rethinkdb-digitalocean/

ipTables (default Ubuntu firewall): https://help.ubuntu.com/community/IptablesHowTo

admin panel set at rethinkdb.mychurchbible.com
https://gist.github.com/jmdobry/6083910
admin uid: deploy
admin pwd: <default>

driver uid: deploy
driver pwd: <default>
http://rethinkdb.com/docs/permissions-and-accounts/

//TODO 
consider creating an SSH tunnel for the driver port as listed in the end of this:
http://rethinkdb.com/docs/security/

and rethinkdb on startup
https://rethinkdb.com/docs/start-on-startup/
