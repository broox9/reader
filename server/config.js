module.exports = {
  rethinkdb: {
    host: 'mychurchbible.com',
    // host: 'localhost',
    port: 28015,
    db: 'bible_data',
    authKey: ''
  },
  app: {
    port: 3010
  }
}
