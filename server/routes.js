var r = require('rethinkdb');
var dbConnection = require('./db/connect.js');
var router = require('koa-router')();

router.get('/', function *(next) {
  try {
    var booksCursor = yield r.table('books').orderBy('id').run(this._rdbConn);
    var allBooks = yield booksCursor.toArray();
    yield this.render('index' , { "data": allBooks});
  }
  catch(err) {
    this.body = JSON.stringify(this);
  }

  yield next;
})
.get('/api/:book/:chapter', function *(next) {
  //TODO fix this order/sort
  var book = Number(this.params.book);
  var chapter = Number(this.params.chapter);

  var verses = yield r.table('bible')
  .orderBy({index: r.asc('bookChapterVerse') })
  .filter({book: book, chapter: chapter })
  .coerceTo('array')
  .run(this._rdbConn);
  // var verses = yield verseCursor.toArray();
  this.body = yield verses;
  yield next;
});

module.exports = router;
