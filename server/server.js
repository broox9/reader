var path = require('path');
var fs = require('fs');
var http = require('http');

var app = require('koa')();
var config =  require('./config.js');
var r = require('rethinkdb');
var dbConnection = require('./db/connect.js');
var middlewares = require('koa-middlewares');
var render = require('koa-ejs');
var router = require('./routes.js');
var fetch = require('fetch');

/* = middlewares */
//DB
app.use(dbConnection);

//static files
app.use(middlewares.staticCache(path.join(process.cwd(), 'public')), {
  maxAge: 10 //yes, 10 seconds
});

//cors
app.use(require('koa-cors')());


//html rendering with ejs
render(app, {
  root: path.join(process.cwd(), 'public'),
  layout: false,
});




//routes
app.use(router.routes());

app.use(function* closeConnection(next) {
  this._rdbConn.close();
});


//start the db, then start the app
r.connect(config.rethinkdb , function(err, connection) {
  if (err) {
    console.log('Some connection error:');
    console.log(err.message);
    process.exit(1);
  }

  //i could do some other junk before starting koa like indexing tables or whatevs
  startKoa();
});

function startKoa() {
  app.listen(3010, function() {
    console.log('Reader in running on 3010');
  });
}
