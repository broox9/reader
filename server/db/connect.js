'use-strict'
var r = require('rethinkdb');
var config = require('../config.js');
var http = require('http');

function* createConnection(next) {
  try {
    var connection = yield r.connect(config.rethinkdb);
    this._rdbConn = connection;
  }
  catch (err) {
    this.status = 500;
    this.body = this.HTTP_STATUS['500'];
  }

  yield next;
}

module.exports = createConnection;
