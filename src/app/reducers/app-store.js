import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunkMiddleWare from 'redux-thunk';


// = state literal
const selectorState = {
  book: 0,
  chapterCount: 0,
  chapter: 0,
  verses: []
}

const appState = {
  verses: []
}

const mergedReducers = combineReducers({
  app: appReducer,
  selector: selectorReducer,
  log: logStuffReducer
});

// = export
const appStore = createStore(
  mergedReducers,
  applyMiddleware(thunkMiddleWare)
);

function appReducer(state = appState, action) {
  switch(action.type) {
    case 'RECIEVE_VERSES':
      return {
        ...state,
        verses: action.verses
      }
      break;
    default:
      return state;
  };
}

function selectorReducer(state = selectorState, action) {
  switch(action.type) {
    case 'BOOK_CHANGE':
      const bookIndex = action.bookNumber - 1;
      return Object.assign(state, {
        book: action.bookNumber,
        chapterCount: Reader.data[bookIndex].ch
      });
      break;
    default:
      return state;
  };
}




function logStuffReducer(state = [], action) {
  console.log('[logger] ACTION:', action.type);
  return _sendState(state);
}

// = util
function _sendState(state) {
  return {...state}; //shallow copy, use Object.assign() for deep;
}

export default appStore;
