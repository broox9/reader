'use-strict';
import axios from 'axios';


export { setBookAction, getVersesAction };

function setBookAction(bookNumber) {
  return {
    type: 'BOOK_CHANGE',
    bookNumber
  }
};

// function setChapterAction(selectorState) {
//  return function() {
//    return axios.get()
//     .then((response) => {
//       return {
//         type: 'CHAPTER_CHANGE',
//         verses: response.data,
//         chapter: selectorState.chapter
//       };
//     });
//   }
// }

function requestVerses(selectorState) {
  return {
    type: 'REQUEST_VERSES',
    chapter: selectorState.chapter
  };
}

function recieveVerses(selectorState, verses) {
  return {
    type: 'RECIEVE_VERSES',
    chapter: selectorState.chapter,
    verses
  }
}

//Async Thunk
function getVersesAction(selectorState) {
  return function(dispatch) {
    dispatch(requestVerses(selectorState));
    return axios.get(`/api/${selectorState.book}/${selectorState.chapter}`)
      .then(response => response.data)
      .then(verses => dispatch(recieveVerses(selectorState, verses)))
  }
}
