'use-strict';

import React from 'react';
import ReactDOM from 'react-dom';

import appStore from './reducers/app-store.js';
import { setBookAction, getVersesAction } from './actions/selector-actions.js';

import SearchBar from './components/search-bar.js';
import Selector from './components/selector.js';
import PageBoard from './components/page-board.js';

require('../styles/base.scss');


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state =  appStore.getState();
    this.storeSubscribe = null;
    this.dispatchSelectorChange = this._dispatchSelectorChange.bind(this);
  }

  componentWillUnmount() {
    this.storeSubscribe();
  }

  componentWillMount() {
    this.storeSubscribe = appStore.subscribe(() => {
      this.setState(appStore.getState())
    });
  }

  componentWillUpdate(nextProps, nextState) {
    console.log('About to update', nextState);
  }

  render() {
    return (
      <div id="app-root">
        <Selector books={this.props.books} selectorChange={this.dispatchSelectorChange} />
        <PageBoard verses={this.state.app.verses} />
      </div>
    );
  }

  _dispatchSelectorChange(updatedState, type) {
    if (type === 'book') {

      appStore.dispatch(setBookAction(updatedState));

    } else if (type === 'chapter') {

      appStore.dispatch(getVersesAction(updatedState));

    }

  }

}

ReactDOM.render( <App books={Reader.data} /> ,  document.querySelector('main'));
