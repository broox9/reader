'use-strict';

import React from 'react';

export default class Selector extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      book: 0,
      chapter: 0,
      chapterCount: 0,
    };

    this.changeBook = this._changeBook.bind(this);
    this.changeChapter = this._changeChapter.bind(this);
  }
  render() {
    return (
      <div className="selector">
        <i className="icon-book"></i>
        <SelectToggler bookList={this.props.books} changeBook={this.changeBook} changeChapter={this.changeChapter}/>
      </div>
    );
  }

  _changeBook(e) {
    const bookNumber = parseInt(e.target.value, 10);
    this.setState({
      book: bookNumber,
      chapterCount: this.props.books[(bookNumber - 1)].ch
    }, () => {
      this.props.selectorChange(bookNumber, 'book');
    });

  }

  _changeChapter(e) {
    this.setState({chapter: e.target.value}, () => {
        this.props.selectorChange(this.state, 'chapter');
    })

  }
}

// = Option component
class Option extends React.Component {
  render() {
    return (
      <option value={this.props.bookNumber}>
        {this.props.name}
      </option>
    );
  }
}

// = Custom Selector
class SelectToggler extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 'book',
      currentBook: 1,
      currentChapter: 1,
    };

    this.changeTab = this._changeTab.bind(this);
  }
  render() {
    let listItems = [];
    const bookOptions = this.props.bookList.map((item, index) => {
      const name = item.name || (index + 1);
      return <SelectTogglerItem bookNumber={item.id} key={index} name={name} />
    });

    const chapterOptions = [];
    for (let i = 0; i < 20; i++) {
      chapterOptions.push(<SelectTogglerItem key={i} bookNumber={i + 1} name={i + 1} />);
    }

    switch(this.state.activeTab) {
      case 'chapter':
        listItems = chapterOptions;
        break;
      case 'book':
      default:
        listItems = bookOptions;
    }

    return (
      <div className="select-toggler">
        <div className="select-toggler__selected">Item</div>
        <ul className="select-toggler__tab-header">
          <li ref="bookTab" onClick={() => this.changeTab('book')} className={this.state.activeTab === 'book' ? 'active' : '' }>
            Book
          </li>
          <li ref="chapterTab" onClick={() => this.changeTab('chapter')} className={this.state.activeTab === 'chapter' ? 'active' : '' }>
            Chapter
          </li>
        </ul>
        <ul className="select-toggler__list">
          {listItems}
        </ul>
      </div>
    );
  }

  _changeTab(type) {
    this.setState({activeTab: type});
  }
}

class SelectTogglerItem extends React.Component {
  render() {
    return (
      <li>{this.props.name}</li>
    )
  }
}
