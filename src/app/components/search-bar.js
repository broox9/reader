'use-strict';

import React from 'react'; 

export default class SearchBar extends React.Component {
  render() {
    return (
      <div className="search-bar">
       <input type="search" placeholder="search here" />
       <i className="icon-search"></i>
      </div>
    );
  }
}
