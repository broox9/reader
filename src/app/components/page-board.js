'use-strict';

import React from 'react';

export default class PageBoard extends React.Component {
  render() {
    if (!this.props.verses || !this.props.verses.length) {
      return (<section id="page-board"></section>);
    }

    const verses = this.props.verses.map((v) => {
      return (
        <Verse key={v.verse} verse={v.verse}>
          {v.words}
        </Verse>);
    })

    return (
      <section id="page-board">
        {verses}
      </section>
    )
  }
}

class Verse extends React.Component {
    render() {
      return (
        <p className="verse-item" data-id={this.props.verse}>
          <span className="verse-item__number">
            {this.props.verse}
          </span>
          <span className="verse-item__text">
            {this.props.children}
          </span>
        </p>
      );
    }
}
