"use-strict";

var path = require("path");
var ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {
  context: __dirname,
  entry: {
    bundle: './src/app/app.js',
    vendor: [
      './node_modules/react/dist/react-with-addons.js',
      './node_modules/react-dom/dist/react-dom.js'  
    ]
  },
  output: {
    path: './public/js',
    filename: '[name].js'
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: ['es2015', 'react', 'stage-0']
        }
      },
      {
        test: /\.(scss|css)$/,
        loaders: ["style", "css", "sass"]
        // loaders: ExtractTextPlugin.extract(["css-loader", "sass-loader"])
      },
      {
        test: /\.(eot|svg|ttf|woff)/,
        loaders: ["url"]
      }
    ]
  },
  sassLoader: {
    includePaths: path.resolve(__dirname, './src/styles')
  },
  plugins: [
    // new ExtractTextPlugin('[name].css')
  ]
};